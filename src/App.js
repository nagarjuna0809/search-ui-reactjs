import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Alert } from 'reactstrap';
import SearchContainer from './search/SearchContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Search Comparision</h1>
        </header>
        <Alert color="primary">
          The goal of this exercise is to create a working program to search a set of documents for the given search term or phrase (single token), and return results in order of relevance. 
        </Alert>
        <div id="searchBox">
        </div>
      </div>
    );
  }
}


export default App;
