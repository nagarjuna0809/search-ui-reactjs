import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from "axios";
import {Table} from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import SearchGrid from './SearchGrid'

import {
  Form, FormGroup,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  Input,
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,Alert
 } from 'reactstrap';


export default class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {searchType: 'simple', searchTerm:'',dropdownOpen: false,
      splitButtonOpen: false,selectLabel:'Select Search Method',searchInputValid:true,
      elapsedTime:0,matches:[],
    searchResultElement:''};

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleDropDown = this.toggleDropDown.bind(this);
    this.toggleSplit = this.toggleSplit.bind(this);
    this.searchMethodSelect=this.searchMethodSelect.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  toggleDropDown() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }
  
  searchMethodSelect(event){
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      selectLabel: event.target.innerText,
      searchType: event.target.value
    });
  }

  toggleSplit() {
    this.setState({
      splitButtonOpen: !this.state.splitButtonOpen
    });
  }

  handleSubmit(event) {
    event.preventDefault();
     axios
      .get("http://35.224.53.118:8084/"+this.state.searchType+"?term="+this.state.searchTerm)
      .then(response => {
        // store the new state object in the component's state
        this.setState({
          elapsedTime: response.data.elapsedTimeSec
        });
        this.setState({
          matches: response.data.matches
        });
        console.log(response.data.matches);
        this.searchGrid(response.data);
      })
      .catch(error => console.log(error));
  }

  onKeyPress(event){
    this.setState({
      searchInputValid:!event.target.value.includes(' ')
    });
  }
 searchGrid(searchResult) {
    var content =''
    console.log(searchResult);
      
      content = searchResult.matches.map((item,i) => {
        return <tr key={i}>
            <th scope="row">i</th>
            <td>{item.title}</td>
            <td>{item.matchCount}</td>
          </tr>;
      });
      console.log(content);
    this.setState ({searchResultElement:
        content
    });
  }
  render() {
    return (
      <Container width="50%">
        <Row>
          <Col>
      <Form onSubmit={this.handleSubmit}>
        <FormGroup>
        <InputGroup>
          <InputGroupButtonDropdown addonType="prepend" isOpen={this.state.splitButtonOpen} toggle={this.toggleSplit}>
            <Button outline>{this.state.selectLabel}</Button>
            <DropdownToggle split outline />
            <DropdownMenu>
              <DropdownItem onClick={this.searchMethodSelect} value='simple'>Simple Search</DropdownItem>
              <DropdownItem onClick={this.searchMethodSelect} value='regex'>Regex Search</DropdownItem>
              <DropdownItem onClick={this.searchMethodSelect} value='solrIndex'>Solr Index Search</DropdownItem>
            </DropdownMenu>
          </InputGroupButtonDropdown>
          <Input placeholder="Enter one term" name='searchTerm' onKeyPress={this.onKeyPress} onChange={this.handleInputChange} valid={this.state.searchInputValid}/>
          <InputGroupAddon addonType="append">
              <Button color="secondary">Search Relevance</Button>
          </InputGroupAddon>
        </InputGroup>
        </FormGroup>
      </Form>
      </Col>
      </Row>
      <Row> <Col> <SearchGrid searchResult={this.state.matches} /> </Col> </Row>
      <Row> <Col>
        <Alert color="success">Total ElapsedTime => {this.state.elapsedTime}</Alert>
      </Col> </Row>
   </Container>
    );
  }
  
}

  