import React, { Component } from 'react';
import {Table} from 'reactstrap';
import PropTypes from 'prop-types';

export default class SearchGrid extends Component {

    render() {
        var content;
        var matches = this.props.searchResult;
 
        content = matches.map((item,i) => {
        return <tr>
            <th scope="row">{i+1}</th>
            <td>{item.title}</td>
            <td>{item.matchCount} </td>
          </tr>;
        });

        return ( 
        <Table >
            <thead>
          <tr>
            <th>#</th>
            <th>File Name</th>
            <th>Match Count</th>
          </tr>
        {content}
        </thead>
        </Table>
        );
    }
  }
  
 SearchGrid.propTypes = {
      searchResult: PropTypes.array.isRequired
  };
