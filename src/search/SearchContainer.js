import React, { Component } from 'react';
import axios from "axios";
import SearchFilter from './SearchFilter';
import {Table} from 'reactstrap';
import PropTypes from 'prop-types';

export default class SearchContainer  extends Component {

  constructor(props) {
    super(props);
    this.state = {searchResult:[]};

    this.handleFilterUpdate = this.handleSearchChange.bind(this);
  }

  handleSearchChange(filterValue) {

    axios
      .get("http://localhost:8080/"+filterValue.type+"?term="+filterValue.term)
      .then(response => {
        // store the new state object in the component's state
        this.setState({
          searchResult: response.data
        });

//         const listItems = searchOutPut.map((number) =>
//   <li>{number}</li>
// );

      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <div>
        <SearchFilter onSearchChange={this.handleSearchChange} />
        <SearchGrid searchResult={this.searchResult} />
      </div>
    );
  }
};

SearchContainer.propTypes = {
    handleSearchChange: PropTypes.func
};

function SearchGrid(props) {
    var content =''
      content = props.searchResult.map((item) => {
        return <tr>
            <th scope="row"></th>
            <td>{item.title}</td>
            <td>{item.matchCount}</td>
          </tr>;
      });
    return (
      <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>File Name</th>
            <th>Match Count</th>
          </tr>
        {content}
        </thead>
        </Table>
    );
  }
