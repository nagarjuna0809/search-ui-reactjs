import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import SearchContainer from './search/SearchContainer';
import SearchFilter from './search/SearchFilter';
 
ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(
  <SearchFilter />,
  document.getElementById('searchBox')
);

registerServiceWorker();
